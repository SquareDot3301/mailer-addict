Module {
	name: 'mailer-addict'
	description: 'A markdown, free, open source mail client written in V'
	version: '0.0.1'
	license: 'MIT'
	dependencies: [
		"markdown",
		"ui"
	]
}
