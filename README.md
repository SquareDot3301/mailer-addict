<p align="center">
  <img src="/docs/logo.png" alt="Sublime's custom image"/>
  <h1 align="center">📧 Mailer Addict 📧</h1>
</p>

---

# Mailer Addict - A new mail client

---

Mailer Addict is a little project by [SquareDot 3301](https://gitlab.com/SquareDot3301) written in VLang, a new language very cool !

## Features

- [x] Send mail in SMTP with SSL
- [x] Save personnal informations
- [x] Send mail with markdown
- [ ] Get all mails

## How to contribute ?

_You_ can contribute to MailerAddict by :

- Open an issue or pull request
- Fork the repository to update or modify the code
- Just discuss with me ^^

## How MailerAddict works ?

Please see [this file](/docs/how-maileraddict-works.md) to know how MailerAddict works.

## License

This software is under license MIT, you can use and modify
