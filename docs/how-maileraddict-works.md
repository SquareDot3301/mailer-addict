# How MailerAddict works ?

- [Fundamentals](#basic)
  - [V Language](#v-language)
  - [Packages used](#packages-used)
- [Architecture](#architecture)
  - [Root](#root)
  - [Src folder](#src-folder)
    - [Assets folder](#assets-folder)
    - [Test folder](#test-folder)
  - [Docs folder](#docs-folder)
  - [Bin folder](#bin-folder)

## Basic

Mailer addict is written in VLang, a pretty new language.

### V Language

VLang is **very fast, very secure, C based**.

If you want to know more about VLang, it's [here](https://github.com/vlang/v)

But if you just want to install V to run MailerAddict's code :

```
git clone https://github.com/vlang/v
cd v
make
# HINT: Using Windows? run make.bat in a cmd shell, or ./make.bat in PowerShell
```

PS : you will need a C Compiler before install VLang, install [TCC Compiler](https://repo.or.cz/w/tinycc.git)

Congrulations, you can code in V !

### Packages Used

Below the packages used for Mailer Addict :

- [V Ui](https://github.com/vlang/ui)
- [V Markdown](https://github.com/vlang/markdown)

## Architecture

### Root

- .gitignore & .gitattributes & .gitlab-ci.yml
  Used for operation of git & gitlab CI
- README.md
  The presentation of Mailer Addict
- LICENSE
  The license's file
- Makefile
  The shortcuts to run Mailer Addict or build it
- v.mod
  File generate by VLang (like package.json)

**Nothing important here**

### Src folder

- [main.v](/src/main.v)
  The main file of Mailer Addict -> Contain the settings of the App for V Ui
- [register.v](/src/register.v)
  The file who create a data.db with register credentials (email, password of the email, SMTP server, port of the SMTP server, the name (email adress), is SSL (true or false))
- [send_mail.v](/src/send_mail.v)
  The file used to send mail with the file `data.db` created by `register.v`
- [view_profile.v](/src/view_profile.v)
  The function open a box message with your credentials
- [btn_help.v](/src/btn_help.v)
  The function open a box message with the informations of Mailer Addict

#### Assets folder

- [MailerAddict.png](/src/assets/MailerAddict.png)
  The logo of Mailer Addict

#### Test folder

In progress...

### Docs folder

Contain a copy of the logo of Mailer Addict & this file

### Bin folder

Contain the build of Mailer Addict
